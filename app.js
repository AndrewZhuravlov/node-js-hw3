const express = require('express');
const app = express();

const mongoose = require('mongoose');
const { dbConnect, mainErrHandler, badReq } = require('./helpers');
dbConnect(mongoose);
app.use(express.json());
require('dotenv').config();
const authRouter = require('./routes/authRouter');
const profileRouter = require('./routes/profileRouter');
const trucksRouter = require('./routes/trucksRouter');
const truckByIdRouter = require('./routes/truckByIdRouter');
const loadsRouter = require('./routes/loadsRouter');
const loadByIdRouter = require('./routes/loadByIdRouter');
const MONGO_MORGAN = require('./helpers/logger');

app.use(MONGO_MORGAN)
app.use(require('morgan')('dev'));
app.use('/api/auth', authRouter);
app.use('/api/users', profileRouter);
app.use('/api/trucks', trucksRouter, truckByIdRouter);
app.use('/api/loads', loadsRouter, loadByIdRouter);
app.use(badReq);
app.use(mainErrHandler);

module.exports = app;

/* eslint-disable camelcase */
const { successRes, messageToLoadLog } = require('../../helpers');
const {
  findUserLoadById,
  findUserLoadAndUpdate,
  deleteLoad,
  findUserLoadCriteria,
} = require('../../models/dao/loadDAO');
const { findTruckByCriteria } = require('../../models/dao/truckDAO');
const {
  posted,
  newLoad,
  assigned,
} = require('config').get('UberTruck.loadStatus');
const { inService } = require('config').get('UberTruck.trucksStatuses');
const [enRoute] = require('config').get('UberTruck.loadState');

const getLoadById = async (req, res) => {
  const { loadId, userId } = req.identifiers;
  const load = await findUserLoadById(loadId, userId);

  successRes({ load }, res);
};

const updateLoadById = async (req, res) => {
  const { loadId, userId } = req.identifiers;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  await findUserLoadAndUpdate({
    _id: loadId,
    created_by: userId,
  }, {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    '$push': { 'logs': messageToLoadLog('Load has been updated') },
  });

  successRes({ message: 'Load has been updated' }, res);
};

const deleteLoadById = async (req, res) => {
  const { loadId, userId } = req.identifiers;

  await deleteLoad({ created_by: userId, _id: loadId, status: newLoad });
  // тут должен быть deleteCount и проверка
  successRes({ message: 'Truck has been deleted' }, res);
};

const postLoad = async (req, res) => {
  const { loadId, userId } = req.identifiers;

  await findUserLoadAndUpdate(
    { _id: loadId, created_by: userId },
    { status: posted },
  );
  const load = await findUserLoadById(loadId, userId);
  const { width, height, length } = load.dimensions;
  const loadVolume = width * height * length;
  const truck = await findTruckByCriteria({
    'status': inService,
    'capacity.payload': { $gte: load.payload },
    'assigned_to': { $ne: 'NA' },
    'capacity.volume': { $gte: loadVolume },
  });

  if (!truck.length) {
    await findUserLoadAndUpdate(
      { _id: loadId, created_by: userId },
      { status: newLoad },
    );
    return successRes({
      message: 'Load posted successfully but driver was not found',
      driver_found: truck.length ? true : false,
    }, res);
  }
  load['status'] = assigned;
  load['assigned_to'] = truck[0]['created_by'];
  load['state'] = enRoute;
  load.logs.push(messageToLoadLog(
    `Load assigned to driver with id ${load.assigned_to}`,
  ));
  await truck[0].save();
  await load.save();

  successRes({
    message: 'Load posted successfully',
    driver_found: truck.length ? true : false,
  }, res);
};

const showShippingInfo = async (req, res) => {
  const { loadId, userId } = req.identifiers;
  const load = await findUserLoadCriteria({
    _id: loadId,
    created_by: userId,
  });

  if (!load) {
    return failRequest('No shipped info was found', res);
  }

  load.logs.push(messageToLoadLog('Owner requested shipper info'));
  const truck = await findTruckByCriteria({ created_by: load.assigned_to });
  successRes({ truck, load }, res);
  await load.save();
};

module.exports = {
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoad,
  showShippingInfo,
}
  ;

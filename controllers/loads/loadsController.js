/* eslint-disable camelcase */
const Load = require('../../models/loadModel');
const {successRes} = require('../../helpers');
const {limitBarrier, limitDefault, offsetDefault} = require('../../constants');
const {newLoad} = require('config').get('UberTruck.loadStatus');

const addLoadForUSer = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const {_id} = req.userInfo;
  const load = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: _id,
  });

  await load.save();
  successRes({message: 'Load has been created'}, res);
};
const getAllLoads = async (req, res) => {
  const {
    limit = limitDefault,
    offset = offsetDefault,
    status } = req.query;
  const {_id} = req.userInfo;

  if(!status) {
    const loads = await Load.find({created_by: _id}, {__v: 0}, {
      limit: +limit > limitBarrier ? limitDefault : +limit,
      skip: +offset,
      sort: {
        created_date: -1,
      },
    });
    successRes({
      message: 'Success',
      loads,
      limit,
      offset: +limit + +offset,
    }, res);
  }
  
  const loads = await Load.find({created_by: _id, 
    status}, {__v: 0}, {
    limit: +limit > limitBarrier ? limitDefault : +limit,
    skip: +offset,
    sort: {
      created_date: -1,
    },
  });
  successRes({
    message: 'Success',
    loads,
    limit,
    offset: +limit + +offset,
  }, res);
};

module.exports = {
  addLoadForUSer,
  getAllLoads,
}
;

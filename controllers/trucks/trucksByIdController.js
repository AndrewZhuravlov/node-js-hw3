const {successRes, capacityDefiner, failRequest} = require('../../helpers');
const {
  findUserTruckById,
  deleteTruck,
  assignTruck,
} = require('../../models/dao/truckDAO');
const Truck = require('../../models/truckModel');
const {inService} = require('config').get('UberTruck.trucksStatuses');
const getTruckById = async (req, res) => {
  const {truckId, userId} = req.identifiers;
  const truck = await findUserTruckById(truckId, userId);

  successRes({truck}, res);
};

const updateTruckById = async (req, res) => {
  const {truckId, userId} = req.identifiers;
  const {type} = req.body;
  const truck = await findUserTruckById(truckId, userId);

  truck.type = type;
  truck.capacity = await capacityDefiner(type);
  truck.save();
  successRes({message: 'Truck has been updated!'}, res);
};

const deleteTruckById = async (req, res) => {
  const {truckId, userId} = req.identifiers;

  await deleteTruck(truckId, userId);
  successRes({message: 'Truck has been deleted'}, res);
};

const assignTruckToUser = async (req, res) => {
  const {truckId, userId} = req.identifiers;

  const truck = await Truck.findOne({
    created_by: userId,
    assigned_to: {$ne: 'NA'},
  });

  if (!truck) {
    await assignTruck({_id: truckId}, {assigned_to: userId});
    return successRes({message: 'Truck has been assigned'}, res);
  }

  if (truck.status !== inService) {
    failRequest('I cant assigned driver for another truck', res);
  }

  truck.assigned_to = 'NA';
  await truck.save();
  await assignTruck({_id: truckId}, {assigned_to: userId});
  return successRes({message: 'Truck has been assigned'}, res);
};

module.exports = {
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckToUser,
}
;

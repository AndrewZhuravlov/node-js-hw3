const Truck = require('../../models/truckModel'); ;
const {capacityDefiner, failRequest, successRes} = require('../../helpers');
const {findAllUserTrucks} = require('../../models/dao/truckDAO');

const addTruckForUSer = async (req, res) => {
  const {_id} = req.userInfo;
  const {type} = req.body;

  if (!type) {
    return failRequest('Type should be defined', res);
  }

  const uppercaseType = type.toUpperCase() || '';
  const truck = new Truck({
    type: uppercaseType,
    created_by: _id,
    capacity: capacityDefiner(uppercaseType),
  });
  await truck.save();
  successRes({message: 'Truck has been created'}, res);
};
const getAllUsersTrucks = async (req, res) => {
  const {_id} = req.userInfo;
  const trucks = await findAllUserTrucks(_id);

  successRes({trucks}, res);
};

module.exports = {
  addTruckForUSer,
  getAllUsersTrucks,
}
;

const {Router} = require('express');
// eslint-disable-next-line new-cap
const route = Router();
const {
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoad,
  showShippingInfo,
} = require('../controllers/loads/loadsByIdController');
const {asyncErrHandler} = require('../helpers');
const {doesLoadExist} = require('../middlewares/doesDocumentExist');
const {loadInfoValidation} = require('../middlewares/inputValidation');
const jwtVer = require('../middlewares/jwtVer');
const {doesUserShipper} = require('../middlewares/roleChecker');

route.get('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(doesLoadExist),
    asyncErrHandler(getLoadById));


route.get('/:id/shipping_info',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(doesLoadExist),
    asyncErrHandler(showShippingInfo));

route.put('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(loadInfoValidation),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(doesLoadExist),
    asyncErrHandler(updateLoadById));

route.delete('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(doesLoadExist),
    asyncErrHandler(deleteLoadById));

route.post('/:id/post',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(doesLoadExist),
    asyncErrHandler(postLoad));

module.exports = route;

const { Router } = require('express');
// eslint-disable-next-line new-cap
const route = Router();
const {
    addLoadForUSer,
    getAllLoads } = require('../controllers/loads/loadsController');
const { asyncErrHandler } = require('../helpers');
const { user } = require('../middlewares/doesDocumentExist');
const { loadInfoValidation } = require('../middlewares/inputValidation');
const jwtVer = require('../middlewares/jwtVer');
const { doesUserShipper, doesUserDriver } = require('../middlewares/roleChecker');
const {
    getLoadInfForDriver,
    updateState } = require('../controllers/activeController');

route.post('/',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(loadInfoValidation),
    asyncErrHandler(user),
    asyncErrHandler(addLoadForUSer))
    ;

route.get('/',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(user),
    asyncErrHandler(getAllLoads))
    ;

route.get('/active',
    asyncErrHandler(jwtVer),
    asyncErrHandler(user),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(getLoadInfForDriver),
);

route.patch('/active/state',
    asyncErrHandler(jwtVer),
    asyncErrHandler(user),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(updateState),
);

module.exports = route;

const {Router} = require('express');
// eslint-disable-next-line new-cap
const route = Router();
const {
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckToUser,
} = require('../controllers/trucks/trucksByIdController');
const {asyncErrHandler} = require('../helpers');
const {doesTruckExist} = require('../middlewares/doesDocumentExist');
const {truckType} = require('../middlewares/inputValidation');
const jwtVer = require('../middlewares/jwtVer');
const {doesUserDriver} = require('../middlewares/roleChecker');

route.get('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(doesTruckExist),
    asyncErrHandler(getTruckById));

route.put('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(truckType),
    asyncErrHandler(doesTruckExist),
    asyncErrHandler(updateTruckById));

route.delete('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(doesTruckExist),
    asyncErrHandler(deleteTruckById));

route.post('/:id/assign',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(doesTruckExist),
    asyncErrHandler(assignTruckToUser));

module.exports = route;

const {Schema, model} = require('mongoose');
const {inService} = require('config').get('UberTruck.trucksStatuses');
const schema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: 'NA',
  },
  status: {
    type: String,
    default: inService,
    uppercase: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  type: {
    type: String,
    required: true,
    uppercase: true,
  },
  capacity: {
    type: Object,
  },
});

module.exports = model('Truck', schema);
;

const User = require('../userModel');

const userFind = async (email) => {
  return await User.findOne({email});
}
;

const userDelete = async (email) => await User.deleteOne({email});
module.exports = {
  userFind,
  userDelete,
}
;

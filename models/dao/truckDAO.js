
const Truck = require('../truckModel');

const findAllUserTrucks = async (id) => {
  return await Truck.find({ created_by: id }, { __v: 0 });
};
const findUserTruckById = async (truckId, userId) => {
  return await Truck.findOne({ created_by: userId, _id: truckId }, { __v: 0 });
}
  ;
const deleteTruck = async (truckId, userId) => {
  return await Truck.deleteOne({ created_by: userId, _id: truckId });
};

const assignTruck = async (searchParamsObj, changeParamsObj) => {
  return await Truck.updateOne(searchParamsObj, changeParamsObj);
};

const deleteAllTrucks = async (filterObj) => {
  return await Truck.deleteMany(filterObj);
};

const findTruckByCriteria = async (filterObj) => {
  return await Truck.find(filterObj);
};

module.exports = {
  findAllUserTrucks,
  findUserTruckById,
  deleteTruck,
  assignTruck,
  deleteAllTrucks,
  findTruckByCriteria,
};

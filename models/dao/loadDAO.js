const Load = require('../loadModel');

const deleteAllLoads = async (filterObj) => {
  return await Load.deleteMany(filterObj);
}
;

const findUserLoadById = async (loadId, userId) => {
  return await Load.findOne({created_by: userId, _id: loadId}, {__v: 0});
}
;
const findUserLoadCriteria = async (filterObj) => {
  return await Load.findOne(filterObj, {__v: 0});
}
;

const findUserLoadAndUpdate = async (filterObj, updateObj) => {
  return await Load.updateOne(filterObj, updateObj);
};

const deleteLoad = async (filterObj) => {
  return await Load.deleteOne(filterObj);
};

module.exports = {
  deleteAllLoads,
  findUserLoadById,
  findUserLoadAndUpdate,
  deleteLoad,
  findUserLoadCriteria,
}
;

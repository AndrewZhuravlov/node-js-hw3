const JWT = require('jsonwebtoken');
const {httpResponseStatus: {
  authorizationFailed,
}} = require('../constants');
const secret = require('config').get('UberTruck.secret');

module.exports = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(authorizationFailed).json({
      message: 'No Authorization http header found',
    });
  }

  const [tokenType, token] = header.split(' ');

  if (!token || tokenType !== 'JWT') {
    return res.status(authorizationFailed).json({
      message: 'Invalid token type!',
    });
  }

  await JWT.verify(token, secret, (err, data) => {
    if (err) {
      return res.status(authorizationFailed).json({
        message: 'No valid JWT token found!',
      });
    }

    req.userInfo = data;
    next();
  });
};

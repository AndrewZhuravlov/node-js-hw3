const {failRequest, validateObjectId} = require('../helpers');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');
const Load = require('../models/loadModel');

module.exports.user = async (req, res, next) => {
  const email = req.userInfo ? req.userInfo.email :
    req.body.email;

  if (!email) {
    return failRequest('Set an email', res);
  }

  const ifExist = await User.exists({email});

  if (!ifExist) {
    return failRequest('User does not exist', res);
  }

  next();
}
;

module.exports.doesTruckExist = async (req, res, next) => {
  const truckId = req.params.id;
  const userId = req.userInfo._id;

  if ( !validateObjectId(truckId) || !validateObjectId(userId)) {
    return failRequest('Invalid ID given', res);
  }

  if (! await Truck.exists({_id: truckId, created_by: userId})) {
    return failRequest('Truck does not exist', res);
  }

  req.identifiers = {truckId, userId};
  next();
};

module.exports.doesLoadExist = async (req, res, next) => {
  const loadId = req.params.id;
  const userId = req.userInfo._id;

  if ( !validateObjectId(loadId) || !validateObjectId(userId)) {
    return failRequest('Invalid ID given', res);
  }

  if (! await Load.exists({_id: loadId, created_by: userId})) {
    return failRequest('Load does not exist', res);
  }

  req.identifiers = {loadId, userId};
  next();
};

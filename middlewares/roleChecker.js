const {failRequest} = require('../helpers');
const [shipper, driver] = require('config').get('UberTruck.userRole');

const doesUserDriver = async (req, res, next) => {
  const {role} = req.userInfo;
  console.log(role);
  if (role !== driver) {
    return failRequest('This action do not supported for your role', res);
  }

  next();
}
;

const doesUserShipper = async (req, res, next) => {
  const {role} = req.userInfo;

  if (role !== shipper) {
    return failRequest('This action do not supported for your role', res);
  }

  next();
}
;

module.exports = {
  doesUserDriver,
  doesUserShipper,
};


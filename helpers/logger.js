const MONGOOSE_MORGAN = require('mongoose-morgan');
const CONFIG = require('config');
const {dbUser, dbPassword, dbName} = CONFIG.get('UberTruck.dbConfig');
const MONGO_MORGAN = MONGOOSE_MORGAN(  
      {   
       collection: 'request-logs',    
       connectionString: `mongodb+srv://${dbUser}:${dbPassword}@cluster0.bc5og.mongodb.net/${dbName}`  
      },

      {},
   'tiny'
); 

module.exports = MONGO_MORGAN;